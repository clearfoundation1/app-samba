<?php

$lang['samba_administrator_account'] = 'Обліковий запис адміністратора';
$lang['samba_app_description'] = 'Мережа Windows (Samba) забезпечує необхідне склеювання для взаємодії з системами Windows. Додаток надає послуги автентифікації, файлового сервера і друку, а також керування доменом Windows.';
$lang['samba_app_name'] = 'Мережа Windows (Samba)';
$lang['samba_computers'] = 'Комп`ютери';
$lang['samba_driver_has_not_been_initialized'] = 'Драйвер не був ініціалізований.';
$lang['samba_file_share_tooltip'] = 'Якщо ви хочете створити файли спільного доступу по мережі, скористайтеся додатком Flexshare.';
$lang['samba_initializing_core_system'] = 'Ініціалізація основної системи, будь ласка, наберіться терпіння.';
$lang['samba_initializing_warning'] = 'Ініціалізація може зайняти хвилину або дві, будь ласка, наберіться терпіння.';
$lang['samba_master_node_needs_initialization'] = 'Головний вузол необхідно ініціалізувати, перш ніж продовжити.';
$lang['samba_mode'] = 'Режим';
$lang['samba_samba_directory_installed_warning'] = 'Програмне забезпечення Samba Directory встановлено, тому цей додаток вам більше не потрібен.';
